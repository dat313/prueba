@extends('layout')
@section('content')

    <div class="page page-forms-common">

        <div class="pageheader">
            <h2> {{ trans("AN_FONACOT") }} <span></span></h2>
        </div>
        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile">

                    <!-- tile header -->
                    <div class="tile-header dvd dvd-btm bg-slategray">
                        <h2 class="custom-font">
                            {{ trans("Edición de número de ") }} <strong>{{ trans ("FONACOT") }}</strong>
                        </h2>
                    </div>
                    <!-- /tile header -->

                    <!-- tile body -->
                    <div class="tile-body">
                        <form method="post" action="{{url('base/AN_FONACOT/'.$AN_FONACOT->NUM_EMP)}}" id="myForm">
                            {{method_field('PUT')}}

                            <h4>
                                <strong>
                                    {{$AN_FONACOT->AN_EMP->NUM_EMP}}
                                </strong>
                                / {{$AN_FONACOT->AN_EMP->getNombreCompleto()}}
                            </h4>
                            <div class="clearfix pb-10 mb-10 b-b b-solid"></div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label>{{trans('Número FONACOT')}}</label>

                                    <input type="text" class="form-control" name="NUM_FONACOT" value="{{old('NUM_FONACOT', $AN_FONACOT->NUM_FONACOT)}}" required/>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">

                                    <button type="reset"
                                            class="btn btn-danger btn-ef btn-ef-3 btn-ef-3c mb-10">
                                        <i class="fa fa-refresh"></i> {{trans("Cancelar")}}
                                    </button>
                                    <button type="submit"
                                            class="btn btn-success btn-ef btn-ef-3 btn-ef-3c mb-10">
                                        <i class="fa fa-arrow-right"></i> {{trans("Guardar")}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <!-- /tile body -->

                </section>
                <!-- /tile -->
            </div>
            <!-- /col -->
        </div>
        <!-- /row -->
    </div>
    <!-- Splash Modal -->



@endsection

@section('scripts')
    {!! $validator !!}}
@endsection