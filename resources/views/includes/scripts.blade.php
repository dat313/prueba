<script>

    $("[data-delete-btn]").click(function (e) {
        e.preventDefault();
        var url = $(this).closest('form');

        swal({
            title: "{{ trans("Borrar información")}}",
            text: "{{trans("¿Estás seguro/a que deseas realizar esta acción?")}}",
            type: "warning",
            html: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "{{ trans("Confirmar") }}",
            cancelButtonText: "{{ trans("Cancelar") }}",
            closeOnConfirm: true
        }, function (isConfirm) {
            if (isConfirm) {
                url.submit();
            }
        });
    });

</script>