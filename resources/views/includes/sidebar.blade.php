<aside id="sidebar">


    <div id="sidebar-wrap">

        <div class="panel-group slim-scroll" role="tablist">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="/#sidebarNav">
                            MENU <i class="fa fa-angle-up"></i>
                        </a>
                    </h4>
                </div>
                <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                    <div class="panel-body">


                        <!-- ===================================================
                        ================= NAVIGATION Content ===================
                        ==================================================== -->
                        <ul id="navigation">
                            <li>
                                <a role="button" tabindex="0"><i class="fa fa-list"></i>
                                    <span>{{trans("Registro")}}</span></a>
                                <ul>
                                    <li><a href="/"><i
                                                    class="fa fa-caret-right"></i> {{trans("Registro de Empleados")}}</a>
                                    </li>


                                </ul>
                            </li>




                        </ul>
                        <!--/ NAVIGATION Content -->


                    </div>
                </div>
            </div>
        </div>

    </div>
</aside>