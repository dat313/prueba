<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Nómina Web :: Humanet HR</title>
    <link rel="icon" type="image/ico" href="/assets/images/favicon.ico"/>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ============================================
    ================= Stylesheets ===================
    ============================================= -->
    <!-- vendor css files -->
    <link rel="stylesheet" href="/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/vendor/animate.css">
    <link rel="stylesheet" href="/assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/js/vendor/animsition/css/animsition.min.css">
    <link rel="stylesheet" href="/assets/js/vendor/daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="/assets/js/vendor/morris/morris.css">
    <link rel="stylesheet" href="/assets/js/vendor/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="/assets/js/vendor/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="/assets/js/vendor/rickshaw/rickshaw.min.css">
    <link rel="stylesheet" href="/assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="/assets/js/vendor/datatables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="/assets/js/vendor/datatables/datatables.bootstrap.min.css">
    <link rel="stylesheet" href="/assets/js/vendor/chosen/chosen.css">
    <link rel="stylesheet" href="/assets/js/vendor/summernote/summernote.css">
    <link rel="stylesheet" href="/assets/js/vendor/sweetalert/sweetalert.css">
    <link rel="stylesheet" href="/assets/js/vendor/dropzonejs/dropzone.css">
    <link rel="stylesheet" href="/assets/js/vendor/tagsinput/bootstrap-tagsinput.css">

    <!--Modificacion del CSS Datatable-->


    {{--<link rel="stylesheet" href="/assets/js/angular-ui-tree.css">--}}
    {{--<link rel="stylesheet" href="/assets/js/angular-ui-tree.js">--}}
    {{--<link rel="stylesheet" href="/assets/js/angular.min">--}}



    <link rel="stylesheet" href="/assets/js/vendor/angular-loading-bar-master/build/loading-bar.min.css"/>

    <!-- project main css files -->

    <link rel="stylesheet" href="/assets/css/main.css">
    <link rel="stylesheet" href="/assets/css/custom.css">

    <!--/ stylesheets -->


    <!-- ==========================================
    ================= Modernizr ===================
    =========================================== -->
    <script src="/assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <!--/ modernizr -->


</head>


<body id="minovate" class="appWrapper" ng-app="app">


<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->


<!-- ====================================================
================= Application Content ===================
===================================================== -->
@yield('wrapper_top')
<div id="wrap" class="animsition">


    <!-- ===============================================
================= HEADER Content ===================
================================================ -->
    <section id="header">
        <header class="clearfix">

            <!-- Branding -->
            <div class="branding">
                <a class="brand" href="/index.html">
                    <span>Nómina Web</span>
                </a>
                <a role="button" tabindex="0" class="offcanvas-toggle visible-xs-inline"><i class="fa fa-bars"></i></a>
            </div>
            <!-- Branding end -->


            <!-- Left-side navigation -->
            <ul class="nav-left pull-left list-unstyled list-inline">
                <li class="sidebar-collapse divided-right">
                    <a role="button" tabindex="0" class="collapse-sidebar">
                        <i class="fa fa-outdent"></i>
                    </a>
                </li>
                <li class="dropdown divided-right settings">
                    <a role="button" tabindex="0" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-cog"></i>
                    </a>
                    <ul class="dropdown-menu with-arrow animated littleFadeInUp" role="menu">
                        <li>

                            <ul class="color-schemes list-inline">
                                <li class="title">Header Color:</li>
                                <li><a role="button" tabindex="0" class="scheme-drank header-scheme"
                                       data-scheme="scheme-default"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-black header-scheme"
                                       data-scheme="scheme-black"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-greensea header-scheme"
                                       data-scheme="scheme-greensea"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-cyan header-scheme"
                                       data-scheme="scheme-cyan"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-lightred header-scheme"
                                       data-scheme="scheme-lightred"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-light header-scheme"
                                       data-scheme="scheme-light"></a></li>
                                <li class="title">Branding Color:</li>
                                <li><a role="button" tabindex="0" class="scheme-drank branding-scheme"
                                       data-scheme="scheme-default"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-black branding-scheme"
                                       data-scheme="scheme-black"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-greensea branding-scheme"
                                       data-scheme="scheme-greensea"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-cyan branding-scheme"
                                       data-scheme="scheme-cyan"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-lightred branding-scheme"
                                       data-scheme="scheme-lightred"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-light branding-scheme"
                                       data-scheme="scheme-light"></a></li>
                                <li class="title">Sidebar Color:</li>
                                <li><a role="button" tabindex="0" class="scheme-drank sidebar-scheme"
                                       data-scheme="scheme-default"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-black sidebar-scheme"
                                       data-scheme="scheme-black"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-greensea sidebar-scheme"
                                       data-scheme="scheme-greensea"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-cyan sidebar-scheme"
                                       data-scheme="scheme-cyan"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-lightred sidebar-scheme"
                                       data-scheme="scheme-lightred"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-light sidebar-scheme"
                                       data-scheme="scheme-light"></a></li>
                                <li class="title">Active Color:</li>
                                <li><a role="button" tabindex="0" class="scheme-drank color-scheme"
                                       data-scheme="drank-scheme-color"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-black color-scheme"
                                       data-scheme="black-scheme-color"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-greensea color-scheme"
                                       data-scheme="greensea-scheme-color"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-cyan color-scheme"
                                       data-scheme="cyan-scheme-color"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-lightred color-scheme"
                                       data-scheme="lightred-scheme-color"></a></li>
                                <li><a role="button" tabindex="0" class="scheme-light color-scheme"
                                       data-scheme="light-scheme-color"></a></li>
                            </ul>

                        </li>

                        <li>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-xs-8 control-label">Fixed header</label>
                                    <div class="col-xs-4 control-label">
                                        <div class="onoffswitch lightred small">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                                   id="fixed-header" checked="">
                                            <label class="onoffswitch-label" for="fixed-header">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-xs-8 control-label">Fixed aside</label>
                                    <div class="col-xs-4 control-label">
                                        <div class="onoffswitch lightred small">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                                   id="fixed-aside" checked="">
                                            <label class="onoffswitch-label" for="fixed-aside">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Left-side navigation end -->


            <!-- Search -->
            <div class="search" id="main-search">
                <input type="text" class="form-control underline-input" placeholder="Search...">
            </div>
            <!-- Search end -->


            <!-- Right-side navigation -->
            <ul class="nav-right pull-right list-inline">

                <li>

                </li>

                <li class="dropdown nav-profile">

                    <a href class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/assets/images/profile-photo.jpg" alt="" class="img-circle size-30x30">
                        <span>ADMINISTRADOR GENERAL DEL SISTEMA <i class="fa fa-angle-down"></i></span>
                    </a>

                    <ul class="dropdown-menu animated littleFadeInRight" role="menu">

                        <li>
                            <a role="button" tabindex="0">
                                <span class="badge bg-greensea pull-right">86%</span>
                                <i class="fa fa-user"></i>Profile
                            </a>
                        </li>
                        <li>
                            <a role="button" tabindex="0">
                                <span class="label bg-lightred pull-right">new</span>
                                <i class="fa fa-check"></i>Tasks
                            </a>
                        </li>
                        <li>
                            <a role="button" tabindex="0">
                                <i class="fa fa-cog"></i>Settings
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a role="button" tabindex="0">
                                <i class="fa fa-lock"></i>Lock
                            </a>
                        </li>
                        <li>
                            <a role="button" tabindex="0">
                                <i class="fa fa-sign-out"></i>Logout
                            </a>
                        </li>

                    </ul>

                </li>

                <li class="toggle-right-sidebar">
                    <a role="button" tabindex="0">
                        <i class="fa fa-comments"></i>
                    </a>
                </li>
            </ul>
            <!-- Right-side navigation end -->


        </header>

    </section>
    <!--/ HEADER Content  -->


    <!-- =================================================
    ================= CONTROLS Content ===================
    ================================================== -->
    <div id="controls">


        <!-- ================================================
        ================= SIDEBAR Content ===================
        ================================================= -->
    @include('includes.sidebar')
    <!--/ SIDEBAR Content -->


        <!-- =================================================
        ================= RIGHTBAR Content ===================
        ================================================== -->
        <aside id="rightbar">

            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="/#users" aria-controls="users" role="tab"
                                                              data-toggle="tab"><i class="fa fa-users"></i></a></li>
                    <li role="presentation"><a href="/#history" aria-controls="history" role="tab" data-toggle="tab"><i
                                    class="fa fa-clock-o"></i></a></li>
                    <li role="presentation"><a href="/#friends" aria-controls="friends" role="tab" data-toggle="tab"><i
                                    class="fa fa-heart"></i></a></li>
                    <li role="presentation"><a href="/#settings" aria-controls="settings" role="tab"
                                               data-toggle="tab"><i class="fa fa-cog"></i></a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="users">
                        <h6><strong>Online</strong> Users</h6>

                        <ul>

                            <li class="online">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/ici-avatar.jpg" alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Ing. Imrich <strong>Kamarel</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Ulaanbaatar, Mongolia</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="online">
                                <div class="media">

                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/arnold-avatar.jpg" alt>
                                    </a>
                                    <span class="badge bg-lightred unread">3</span>

                                    <div class="media-body">
                                        <span class="media-heading">Arnold <strong>Karlsberg</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Bratislava, Slovakia</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>

                                </div>
                            </li>

                            <li class="online">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/peter-avatar.jpg" alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Peter <strong>Kay</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Kosice, Slovakia</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="online">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/george-avatar.jpg" alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">George <strong>McCain</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Prague, Czech Republic</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="busy">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/random-avatar1.jpg"
                                             alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Lucius <strong>Cashmere</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Wien, Austria</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="busy">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/random-avatar2.jpg"
                                             alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Jesse <strong>Phoenix</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Berlin, Germany</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                        </ul>

                        <h6><strong>Offline</strong> Users</h6>

                        <ul>

                            <li class="offline">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/random-avatar4.jpg"
                                             alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Dell <strong>MacApple</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Paris, France</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="offline">
                                <div class="media">

                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/random-avatar5.jpg"
                                             alt>
                                    </a>

                                    <div class="media-body">
                                        <span class="media-heading">Roger <strong>Flopple</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Rome, Italia</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>

                                </div>
                            </li>

                            <li class="offline">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/random-avatar6.jpg"
                                             alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Nico <strong>Vulture</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Kyjev, Ukraine</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="offline">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/random-avatar7.jpg"
                                             alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Bobby <strong>Socks</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Moscow, Russia</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="offline">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/random-avatar8.jpg"
                                             alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Anna <strong>Opichia</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Budapest, Hungary</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="history">
                        <h6><strong>Chat</strong> History</h6>

                        <ul>

                            <li class="online">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/ici-avatar.jpg" alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Ing. Imrich <strong>Kamarel</strong></span>
                                        <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor
                                        </small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="busy">
                                <div class="media">

                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/arnold-avatar.jpg" alt>
                                    </a>
                                    <span class="badge bg-lightred unread">3</span>

                                    <div class="media-body">
                                        <span class="media-heading">Arnold <strong>Karlsberg</strong></span>
                                        <small>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                            dolore eu fugiat nulla pariatur
                                        </small>
                                        <span class="badge badge-outline status"></span>
                                    </div>

                                </div>
                            </li>

                            <li class="offline">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/peter-avatar.jpg" alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Peter <strong>Kay</strong></span>
                                        <small>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                                            deserunt mollit
                                        </small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="friends">
                        <h6><strong>Friends</strong> List</h6>
                        <ul>

                            <li class="online">
                                <div class="media">

                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/arnold-avatar.jpg" alt>
                                    </a>
                                    <span class="badge bg-lightred unread">3</span>

                                    <div class="media-body">
                                        <span class="media-heading">Arnold <strong>Karlsberg</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Bratislava, Slovakia</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>

                                </div>
                            </li>

                            <li class="offline">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/random-avatar8.jpg"
                                             alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Anna <strong>Opichia</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Budapest, Hungary</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="busy">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/random-avatar1.jpg"
                                             alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Lucius <strong>Cashmere</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Wien, Austria</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="online">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" role="button" tabindex="0">
                                        <img class="media-object img-circle" src="/assets/images/ici-avatar.jpg" alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Ing. Imrich <strong>Kamarel</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Ulaanbaatar, Mongolia</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="settings">
                        <h6><strong>Chat</strong> Settings</h6>

                        <ul class="settings">

                            <li>
                                <div class="form-group">
                                    <label class="col-xs-8 control-label">Show Offline Users</label>
                                    <div class="col-xs-4 control-label">
                                        <div class="onoffswitch greensea">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                                   id="show-offline" checked="">
                                            <label class="onoffswitch-label" for="show-offline">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form-group">
                                    <label class="col-xs-8 control-label">Show Fullname</label>
                                    <div class="col-xs-4 control-label">
                                        <div class="onoffswitch greensea">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                                   id="show-fullname">
                                            <label class="onoffswitch-label" for="show-fullname">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form-group">
                                    <label class="col-xs-8 control-label">History Enable</label>
                                    <div class="col-xs-4 control-label">
                                        <div class="onoffswitch greensea">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                                   id="show-history" checked="">
                                            <label class="onoffswitch-label" for="show-history">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form-group">
                                    <label class="col-xs-8 control-label">Show Locations</label>
                                    <div class="col-xs-4 control-label">
                                        <div class="onoffswitch greensea">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                                   id="show-location" checked="">
                                            <label class="onoffswitch-label" for="show-location">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form-group">
                                    <label class="col-xs-8 control-label">Notifications</label>
                                    <div class="col-xs-4 control-label">
                                        <div class="onoffswitch greensea">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                                   id="show-notifications">
                                            <label class="onoffswitch-label" for="show-notifications">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form-group">
                                    <label class="col-xs-8 control-label">Show Undread Count</label>
                                    <div class="col-xs-4 control-label">
                                        <div class="onoffswitch greensea">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                                   id="show-unread" checked="">
                                            <label class="onoffswitch-label" for="show-unread">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>

            </div>

        </aside>
        <!--/ RIGHTBAR Content -->


    </div>
    <!--/ CONTROLS Content -->


    <!-- ====================================================
    ================= CONTENT ===============================
    ===================================================== -->
    <section id="content">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
                @endif
            @endforeach
        </div>


        @yield('content')
    </section>
    <!--/ CONTENT -->


</div>
<!--/ Application Content -->

@yield('modals')
@yield('wrapper_bottom')
<!-- ============================================
============== Vendor JavaScripts ===============
============================================= -->
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>--}}
<script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery/jquery-1.11.2.min.js"><\/script>')</script>

<script src="/assets/js/vendor/bootstrap/bootstrap.min.js"></script>

<script src="/assets/js/vendor/jRespond/jRespond.min.js"></script>

<script src="/assets/js/vendor/d3/d3.min.js"></script>
<script src="/assets/js/vendor/d3/d3.layout.min.js"></script>

<script src="/assets/js/vendor/rickshaw/rickshaw.min.js"></script>

<script src="/assets/js/vendor/sparkline/jquery.sparkline.min.js"></script>

<script src="/assets/js/vendor/slimscroll/jquery.slimscroll.min.js"></script>

<script src="/assets/js/vendor/animsition/js/jquery.animsition.min.js"></script>

<script src="/assets/js/vendor/daterangepicker/moment.min.js"></script>
<script src="/assets/js/vendor/daterangepicker/daterangepicker.js"></script>

<script src="/assets/js/vendor/screenfull/screenfull.min.js"></script>

<script src="/assets/js/vendor/flot/jquery.flot.min.js"></script>
<script src="/assets/js/vendor/flot-tooltip/jquery.flot.tooltip.min.js"></script>
<script src="/assets/js/vendor/flot-spline/jquery.flot.spline.min.js"></script>

<script src="/assets/js/vendor/easypiechart/jquery.easypiechart.min.js"></script>

<script src="/assets/js/vendor/raphael/raphael-min.js"></script>
<script src="/assets/js/vendor/morris/morris.min.js"></script>

<script src="/assets/js/vendor/owl-carousel/owl.carousel.min.js"></script>

<script src="/assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<script src="/assets/js/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="/assets/js/vendor/datatables/extensions/dataTables.bootstrap.js"></script>

<script src="/assets/js/vendor/chosen/chosen.jquery.min.js"></script>

<script src="/assets/js/vendor/summernote/summernote.min.js"></script>

<script src="/assets/js/vendor/coolclock/coolclock.js"></script>
<script src="/assets/js/vendor/coolclock/excanvas.js"></script>
<!--/ vendor javascripts -->

<script src="/assets/js/vendor/angular/1.5.0.min.js"></script>
<script src="/assets/js/vendor/ngstorage/ngstorage.min.js"></script>
<script src="/assets/js/vendor/angular-chosen/angular-chosen.min.js"></script>

<script src="/assets/js/vendor/sweetalert/sweetalert-dev.js"></script>
<script src="/assets/js/vendor/sweetalert/ng-sweetalert.min.js"></script>

<script src="/assets/js/vendor/jquery-autocomplete/jquery-autocomplete.min.js"></script>
<script src="/assets/js/vendor/maskedinput/jquery.maskedinput.min.js"></script>

<script src="/assets/js/vendor/form-wizard/jquery.bootstrap.wizard.js "></script>

<script src="/assets/js/vendor/angular-loading-bar-master/build/loading-bar.min.js"></script>
<script src="/assets/js/vendor/is-loading/is-loading.min.js"></script>
<script src="/assets/js/vendor/filestyle/bootstrap-filestyle.min.js"></script>

<!--//DROPZONEJS//-->
<script src="/assets/js/vendor/dropzonejs/dropzone.js "></script>
<script src="/assets/js/vendor/tagsinput/bootstrap-tagsinput.js"></script>

<script src="/vendor/jsvalidation/js/jsvalidation.min.js"></script>


<!--Seccion del Datatable-->
<script src="/assets/js/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="/assets/js/vendor/buttons/datatables.buttons.min.js"></script>
<script src="/assets/js/vendor/buttons/buttons.flash.min.js"></script>
<script src="/assets/js/vendor/jzip/jzip.min.js"></script>
<script src="/assets/js/vendor/pdfmake/pdfmake.min.js"></script>
<script src="/assets/js/vendor/pdfmake/vfs_fonts.js"></script>
<script src="/assets/js/vendor/buttons/buttons.html5.min.js"></script>
<script src="/assets/js/vendor/buttons/buttons.print.min.js"></script>

<!-- ============================================
============== Custom JavaScripts ===============
============================================= -->
<script src="/assets/js/main.js"></script>
<script src="/assets/js/custom.js"></script>
<!--/ custom javascripts -->
<script>
    app.value("BASEURL", "{{'/'}}");
</script>

<!--DataTable-->
<script>
    $("[datatable]").DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
</script>


@include('includes.scripts')

<!-- ===============================================
============== Page Specific Scripts ===============
================================================ -->
@yield('scripts')
<!--/ Page Specific Scripts -->
</body>
</html>
