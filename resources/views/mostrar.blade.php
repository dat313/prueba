@extends('layout')
@section('content')




    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">
            <!-- tile -->
            <section class="tile">
                <div class="tile-header dvd dvd-btm bg-slategray">
                    <h2 class="custom-font">
                        {{ trans("Empleados") }}

                    </h2>
                </div>


                <!-- tile body -->
                <div class="tile-body">
                    <div class="row">
                        <div class="col-md-12">

                            {{$json_empleados}}




                        </div>
                    </div>


                    <!---->

                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="underline custom-font mb-20"></h4>
                        </div>
                    </div>
                </div>

                <!-- /tile body -->

            </section>
            <!-- /tile -->
        </div>
        <!-- /col -->
    </div>
    <!-- /row -->


@endsection

@section('scripts')
@endsection