@extends('layout')
@section('content')

    <div class="page page-forms-common" ng-controller="RegistroController">


        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <!-- tile -->
                <section class="tile">
                    <div class="tile-header dvd dvd-btm bg-slategray">
                        <h2 class="custom-font">
                            {{ trans("Registro de Empleados") }}

                        </h2>
                    </div>


                    <!-- tile body -->
                    <div class="tile-body">
                        <form method="post" action="{{url("/")}}">


                        <div class="row">
                            <div class="col-md-12">

                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input class="form-control" type="text" name="nombre" ng-model="nombre" required/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Apellido Paterno</label>
                                    <input class="form-control" type="text" name="appaterno" ng-model="appaterno" required/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Apellido Materno</label>
                                    <input class="form-control" type="text" name="apmaterno" ng-model="apmaterno" required/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" type="text" name="email" ng-model="email" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Puesto</label>
                                    <input class="form-control" type="text" name="puesto" ng-model="puesto" required/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Fecha de Nacimiento</label>
                                    <input class="form-control date" type="text" name="fnacimiento" ng-model="fnacimiento" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Domicilio</label>
                                    <input class="form-control" type="text" name="domicilio" ng-model="domicilio" required/>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="underline custom-font mb-20"><strong>Skills</strong> </h4>
                            </div>
                        </div>


                        <!--elementos multivalor-->


                        <div class="row">



                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Nombre </label>
                                    <input class="form-control" type="text" ng-model="nombreskill"/>
                                </div>
                            </div>


                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Calificación</label>
                                    <select class="form-control" ng-model="calificacion">
                                        <option value="">Seleccionar</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>

                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <label>&nbsp;</label>
                                <div class="form-group">
                                    <a class="btn btn-greensea btn-ef btn-ef-3 btn-ef-3c mb-10"
                                       ng-click="guardar()" >
                                        {{trans("Agregar")}} <i
                                                class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>


                        <!--seccion de tabla Angular-->
                        <div class="row">
                            <div class="col-md-6 mb-20 grid mt-20">
                                <table class="table table-striped table-bordered" id="basic-usage" ng-show="arraySkill.length > 0 ? true :  true">

                                    <thead>
                                    <tr >
                                        <th>{{trans ("Nombre")}}</th>
                                        <th>{{trans ("Calificación")}}</th>
                                        <th>{{trans ("Acciones")}}</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr ng-repeat="obj in arraySkill">

                                        <td>@{{obj.nombreskill}}</td>
                                        <input type="hidden" name="nombreskill[]" value="@{{obj.nombreskill}}">
                                        <td>@{{obj.calificacion}}</td>
                                        <input type="hidden" name="calificacion[]" value="@{{obj.calificacion}}">
                                        <td class="hidden-print">
                                            <a data-delete-btn
                                               class="btn btn-danger btn-xs hidden-print" ng-click="borrar_skill($index)">
                                                <i class="fa fa-trash"></i>
                                                Eliminar
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!---->

                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="underline custom-font mb-20"></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right mt-10">
                                <label>&nbsp;</label>

                                <div class="form-group">
                                    <a href="{{url('mostrar')}}" class="btn btn-primary btn-ef btn-ef-3 btn-ef-3c mb-10 ">
                                        {{trans("Mostrar")}}<i
                                                class="fa fa-plus"></i></a>
                                    <button type="submit" value="Submit"
                                            class="btn btn-success btn-ef btn-ef-3 btn-ef-3c mb-10">
                                        Guardar <i class="fa fa-save"></i></button>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="underline custom-font mb-20"><strong>Empleados Registrados</strong> </h4>
                            </div>
                        </div>
                        <div class="row" ng-repeat="row in arrayDatosEmp">


                            <div class="col-md-6">

                                <label class="control-label text-success">Datos Personales</label><br>
                                <span><strong>Nombre: </strong> @{{row.nombre}}</span><br>
                                <span><strong>Ap. Paterno: </strong> @{{row.appaterno}}</span><br>
                                <span><strong>Ap. Materno: </strong> @{{row.apmaterno}}</span><br>
                                <span><strong>Email: </strong> @{{row.email}}</span><br>
                                <span><strong>Puesto: </strong> @{{row.puesto}}</span><br>
                                <span><strong>Fecha Nac: </strong> @{{row.fnacimiento}}</span><br>
                                <span><strong>Domicilio: </strong> @{{row.domicilio}}</span><br>
                            </div>


                            <div class="col-md-6">
                                <label class="control-label text-success">Skill</label><br>
                                <div class="row" ng-repeat="datos in row.arraySkillEmp ">
                                    <div class="col-md-12">

                                        <span ><strong>Nombre: </strong> @{{ datos.nombreskill }}</span><br>
                                        <span ><strong>Calificación: </strong> @{{ datos.calificacion }}</span><br>
                                    </div>
                                </div>



                            </div>
                            <div class="col-md-12">

                                <h4 class="underline custom-font mb-20"></h4>
                            </div>
                        </div>
                        </form>
                    </div>

                    <!-- /tile body -->

                </section>
                <!-- /tile -->
            </div>
            <!-- /col -->
        </div>
        <!-- /row -->
    </div>
    <!-- Splash Modal -->

@endsection

@section('scripts')

    <script>
        app.controller('RegistroController', function ($scope,$http) {

            $scope.arraySkill = [];
            $scope.arrayDatosEmp = [];
           // $scope.arraySkillEmp = [];

            $scope.guardar = function () {
                $scope.arraySkill.push({
                    'nombreskill':$scope.nombreskill,
                    'calificacion': $scope.calificacion
                });

                $scope.nombreskill='';
                $scope.calificacion='';
            }


            $scope.borrar_skill = function (index) {

                $scope.arraySkill.splice(index, 1)
            }


            $scope.guardarEmp = function () {

              //  $scope.arraySkillEmp = $scope.arraySkill;

                $scope.arrayDatosEmp.push({
                    'nombre':$scope.nombre,
                    'appaterno':$scope.appaterno,
                    'apmaterno':$scope.apmaterno,
                    'email':$scope.email,
                    'puesto':$scope.puesto,
                    'fnacimiento':$scope.fnacimiento,
                    'domicilio':$scope.domicilio,
                    'arraySkillEmp':$scope.arraySkill

                });

                $scope.nombreskill='';
                $scope.calificacion='';

                //limpiar datos emp
                $scope.nombre='';
                $scope.appaterno='';
                $scope.apmaterno='';
                $scope.email='';
                $scope.puesto='';
                $scope.fnacimiento='';
                $scope.domicilio='';
                $scope.arraySkill='';

                $scope.arraySkill=[];

    
                $scope.Register();
            }

 //---
 $scope.Register = function() {

    $http.post("/app/Http/Controllers/HomeController.php",
    {
        'name': $scope.arrayDatosEmp
    })
    .success(function(response) 
    {
    $scope.message = response.message;
    });

};   

//---
            $scope.borrar_emp = function (index) {

                $scope.arraySkill.splice(index, 1)
            }
        });

    </script>

@endsection