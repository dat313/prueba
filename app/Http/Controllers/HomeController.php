<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function index()
    {
        return view('home');

        // return view('home');
    }
    public function mostrar()
    {
        $datos_empleados = file_get_contents("empleados.json");
        $json_empleados = json_decode($datos_empleados, true);
        $toview=[
            'json_empleados'=>$json_empleados
        ];
        return View::make('mostrar', $toview);
        //return view('mostrar');

        // return view('home');
    }

    public function store(Request $request)
    {
        $nombre = $request->input('nombre');
        $appaterno = $request->input('appaterno');
        $apmaterno = $request->input('apmaterno');
        $email = $request->input('email');
        $puesto = $request->input('puesto');
        $fnacimiento = $request->input('fnacimiento');
        $domicilio = $request->input('domicilio');
        $nombreskill = $request->input('nombreskill');
        $arraySkill = $request->input('arraySkill');
        $calificacion = $request->input('calificacion');

        $archivo = "empleados.json";
        if(file_exists ( $archivo ))
        {
            $datos_empleados = file_get_contents("empleados.json");
            $json_empleados = json_decode($datos_empleados, true);
        }


        $arr_empleados = array('nombre' => $nombre,
            'appaterno' => $appaterno,
            'apmaterno' => $apmaterno,
            'email' => $email,
            'puesto' => $puesto,
            'fnacimiento' => $fnacimiento,
            'domicilio' => $domicilio,
            'nombreskill' => $nombreskill,
            'calificacion' => $calificacion
        );
        if(file_exists ( $archivo ))
        {
            array_push($json_empleados, $arr_empleados);
            $json_string = json_encode($json_empleados);
        }
        else{
            $json_string = json_encode($arr_empleados);
        }


        $file = 'empleados.json';
        file_put_contents($file, $json_string);
        return redirect('/')->with('alert-success', trans('La acción se realizó con éxito'));

    }

}
