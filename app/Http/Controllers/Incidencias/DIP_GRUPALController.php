<?php

namespace App\Http\Controllers\Incidencias;

use App\GmpLib\Helpers;
use App\Http\Controllers\Base\AN_DIPSController;
use App\Http\Controllers\Base\AN_EMP_INCIController;
use App\Http\Controllers\Base\AN_TAB_CATController;
use App\Http\Controllers\Base\AN_TAB_CCOSTOController;
use App\Http\Controllers\Base\AN_TABULADOR_SALController;
use App\Models\AN_EMP;
use App\Models\AN_EMP_INCI;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class DIP_GRUPALController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $toView = [];
        $AN_DIPS_CONTROLLER = new AN_DIPSController();
        $AN_TAB_CCOSTO = new AN_TAB_CCOSTOController();
        $AN_TABULADOR_SAL = new AN_TABULADOR_SALController();
        $AN_TAB_CAT = new AN_TAB_CATController();
        $toView["dips"] = $AN_DIPS_CONTROLLER->index($request);
        $toView["ccosto"] = $AN_TAB_CCOSTO->index($request);
        $toView["tabulador"] = $AN_TABULADOR_SAL->index($request);
        $toView["categorias"] = $AN_TAB_CAT->index($request);

        return View::make('modulos.incidencias.dip_grupal', $toView);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $NUM_EMP = $request->input("NUM_EMP");
        $incidencias = $request->input("incidencias");

        DB::transaction(function () use ($NUM_EMP, $incidencias) {
            $newArr = [];
            $empleados = AN_EMP::getConPermisos()->get();

            foreach ($incidencias as $arr) {

                if ($arr["tipo_accion"] == "Alta") {
                    foreach ($empleados as $emp) {
                        $NUM_EMP = $emp->NUM_EMP;
                        $inci = AN_EMP_INCI::where("NUM_EMP", $emp->NUM_EMP)
                            ->where("TIPO_NOM", Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)
                            ->where("CLV_DIP", Helpers::getArrVal("CLV_DIP", $arr))
                            ->first();


                        if (!$inci) {
                            $obj = AN_EMP_INCI::create([
                                "NUM_EMP" => $NUM_EMP,
                                "TIPO_NOM" => Helpers::getAsignacionTIPO_NOM()->TIPO_NOM,
                                "CLV_DIP" => Helpers::getArrVal("CLV_DIP", $arr),
                                "PARAM1" => Helpers::getArrVal("PARAM1", $arr, 0),
                                "PARAM2" => Helpers::getArrVal("PARAM2", $arr, 0),
                                "PARAM3_DESDE" => Helpers::fechaViewToOpen(Helpers::getArrVal("PARAM3_DESDE", $arr)),
                                "PARAM3_HASTA" => Helpers::fechaViewToOpen(Helpers::getArrVal("PARAM3_HASTA", $arr)),
                                "REFERENCIA" => Helpers::getArrVal("REFERENCIA", $arr),
                                "CCOSTO" => Helpers::getArrVal("CCOSTO", $arr),
                                "TABULADOR" => Helpers::getArrVal("TABULADOR", $arr),
                            ]);
                            $newArr[] = $obj->toArray();

                            Helpers::bitacora("AN_EMP_INCI",
                                [
                                    "NUM_EMP" => $NUM_EMP,
                                    "TIPO_NOM" => Helpers::getAsignacionTIPO_NOM()->TIPO_NOM,
                                    "CLV_DIP" => Helpers::getArrVal("CLV_DIP", $arr)
                                ],
                                [], $newArr);
                        }
                    }
                }

                if ($arr["tipo_accion"] == "Cambio/Alta") {
                    foreach ($empleados as $emp) {
                        $NUM_EMP = $emp->NUM_EMP;
                        $inci = AN_EMP_INCI::where("NUM_EMP", $emp->NUM_EMP)
                            ->where("TIPO_NOM", Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)
                            ->where("CLV_DIP", Helpers::getArrVal("CLV_DIP", $arr))
                            ->where("REFERENCIA", Helpers::getArrVal("REFERENCIA", $arr))
                            ->first();
                        $oldArr = [];
                        if ($inci) {
                            $oldArr = $inci->toArray();
                            AN_EMP_INCI::where("NUM_EMP", $emp->NUM_EMP)
                                ->where("TIPO_NOM", Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)
                                ->where("CLV_DIP", Helpers::getArrVal("CLV_DIP", $arr))
                                ->where("REFERENCIA", Helpers::getArrVal("REFERENCIA", $arr))
                                ->delete();
                        }
                        $obj = AN_EMP_INCI::create([
                            "NUM_EMP" => $NUM_EMP,
                            "TIPO_NOM" => Helpers::getAsignacionTIPO_NOM()->TIPO_NOM,
                            "CLV_DIP" => Helpers::getArrVal("CLV_DIP", $arr),
                            "PARAM1" => Helpers::getArrVal("PARAM1", $arr, 0),
                            "PARAM2" => Helpers::getArrVal("PARAM2", $arr, 0),
                            "PARAM3_DESDE" => Helpers::fechaViewToOpen(Helpers::getArrVal("PARAM3_DESDE", $arr)),
                            "PARAM3_HASTA" => Helpers::fechaViewToOpen(Helpers::getArrVal("PARAM3_HASTA", $arr)),
                            "REFERENCIA" => Helpers::getArrVal("REFERENCIA", $arr),
                            "CCOSTO" => Helpers::getArrVal("CCOSTO", $arr),
                            "TABULADOR" => Helpers::getArrVal("TABULADOR", $arr),
                        ]);
                        $newArr = $obj->toArray();

                        Helpers::bitacora("AN_EMP_INCI",
                            [
                                "NUM_EMP" => $NUM_EMP,
                                "TIPO_NOM" => Helpers::getAsignacionTIPO_NOM()->TIPO_NOM,
                                "CLV_DIP" => Helpers::getArrVal("CLV_DIP", $arr),
                                "REFERENCIA" => Helpers::getArrVal("REFERENCIA", $arr)
                            ],
                            $oldArr, $newArr);
                    }
                }

                if ($arr["tipo_accion"] == "Siempre Asigna") {
                    foreach ($empleados as $emp) {
                        $NUM_EMP = $emp->NUM_EMP;

                        $obj = AN_EMP_INCI::create([
                            "NUM_EMP" => $NUM_EMP,
                            "TIPO_NOM" => Helpers::getAsignacionTIPO_NOM()->TIPO_NOM,
                            "CLV_DIP" => Helpers::getArrVal("CLV_DIP", $arr),
                            "PARAM1" => Helpers::getArrVal("PARAM1", $arr, 0),
                            "PARAM2" => Helpers::getArrVal("PARAM2", $arr, 0),
                            "PARAM3_DESDE" => Helpers::fechaViewToOpen(Helpers::getArrVal("PARAM3_DESDE", $arr)),
                            "PARAM3_HASTA" => Helpers::fechaViewToOpen(Helpers::getArrVal("PARAM3_HASTA", $arr)),
                            "REFERENCIA" => Helpers::getArrVal("REFERENCIA", $arr),
                            "CCOSTO" => Helpers::getArrVal("CCOSTO", $arr),
                            "TABULADOR" => Helpers::getArrVal("TABULADOR", $arr),
                        ]);
                        $newArr[] = $obj->toArray();

                        Helpers::bitacora("AN_EMP_INCI",
                            [
                                "NUM_EMP" => $NUM_EMP,
                                "TIPO_NOM" => Helpers::getAsignacionTIPO_NOM()->TIPO_NOM,
                                "CLV_DIP" => Helpers::getArrVal("CLV_DIP", $arr)
                            ],
                            [], $newArr);

                    }
                }

                if ($arr["tipo_accion"] == "Borrar Específica") {
                    foreach ($empleados as $emp) {
                        $NUM_EMP = $emp->NUM_EMP;
                        $inci = AN_EMP_INCI::where("NUM_EMP", $emp->NUM_EMP)
                            ->where("TIPO_NOM", Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)
                            ->where("CLV_DIP", Helpers::getArrVal("CLV_DIP", $arr))
                            ->where("REFERENCIA", Helpers::getArrVal("REFERENCIA", $arr))
                            ->first();
                        $oldArr = [];
                        if ($inci) {
                            $oldArr = $inci->toArray();
                            AN_EMP_INCI::where("NUM_EMP", $emp->NUM_EMP)
                                ->where("TIPO_NOM", Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)
                                ->where("CLV_DIP", Helpers::getArrVal("CLV_DIP", $arr))
                                ->where("REFERENCIA", Helpers::getArrVal("REFERENCIA", $arr))
                                ->delete();
                        }


                        Helpers::bitacora("AN_EMP_INCI",
                            [
                                "NUM_EMP" => $NUM_EMP,
                                "TIPO_NOM" => Helpers::getAsignacionTIPO_NOM()->TIPO_NOM,
                                "CLV_DIP" => Helpers::getArrVal("CLV_DIP", $arr),
                                "REFERENCIA" => Helpers::getArrVal("REFERENCIA", $arr)
                            ],
                            $oldArr, []);
                    }
                }

                if ($arr["tipo_accion"] == "Borrar DIP") {
                    foreach ($empleados as $emp) {
                        $NUM_EMP = $emp->NUM_EMP;
                        $inciArr = AN_EMP_INCI::where("NUM_EMP", $emp->NUM_EMP)
                            ->where("TIPO_NOM", Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)
                            ->where("CLV_DIP", Helpers::getArrVal("CLV_DIP", $arr))
                            ->get();
                        foreach ($inciArr as $inci) {
                            $oldArr = [];
                            if ($inci) {
                                $oldArr = $inci->toArray();
                                AN_EMP_INCI::where("NUM_EMP", $emp->NUM_EMP)
                                    ->where("TIPO_NOM", Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)
                                    ->where("CLV_DIP", Helpers::getArrVal("CLV_DIP", $arr))
                                    ->delete();
                            }


                            Helpers::bitacora("AN_EMP_INCI",
                                [
                                    "NUM_EMP" => $NUM_EMP,
                                    "TIPO_NOM" => Helpers::getAsignacionTIPO_NOM()->TIPO_NOM,
                                    "CLV_DIP" => Helpers::getArrVal("CLV_DIP", $arr),
                                ],
                                $oldArr, []);
                        }
                    }
                }
            }
        });
        $AN_EMP_INCI = new AN_EMP_INCIController();
        return $incidencias;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
