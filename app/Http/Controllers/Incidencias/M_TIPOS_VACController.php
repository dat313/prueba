<?php
/**
 * Created by PhpStorm.
 * User: aicruz
 * Date: 05/09/2017
 * Time: 12:40 PM
 */

namespace App\Http\Controllers\Incidencias;

use App\GmpLib\Helpers;
use App\Models\AN_UNID_MEDIC;
use App\Models\AN_EMP_INCI;
use App\Models\AN_FONACOT;
use App\MOdels\AN_EMP_IMSS;
use App\Models\AN_FONACOT_DET1;
use App\Models\AN_CATALOGOS_DET1;
use App\Models\AN_TAB_PAR_INFO;
use App\Models\AN_TAB_VAC;
use App\Models\AN_TAB_VAC_DET1;
use App\Models\OnwLog;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Proengsoft\JsValidation\Facades\JsValidatorFacade;


class M_TIPOS_VACController extends Controller
{
    public function index(Request $request)
    {
        $CLAVE_TABLA = '';

        if(!empty($request->input('CLAVE_TABLA'))){


            $CLAVE_TABLA = $request->input('CLAVE_TABLA');
        }


        $toView = [
            "CLAVE_TABLA" => $CLAVE_TABLA
        ];

        $AN_TAB_VAC_DET1 = AN_TAB_VAC_DET1::where('CLAVE_TABLA',$CLAVE_TABLA)->get();
        $AN_TAB_VAC =  AN_TAB_VAC::all();
        $toView['AN_TAB_VAC'] = $AN_TAB_VAC;
        $toView['AN_TAB_VAC_DET1'] = $AN_TAB_VAC_DET1;

        return View::make('modulos.incidencias.M_TIPOS_VAC.index',$toView);
    }



    protected function getValidations()
    {
    }
    public function create(Request $request)
    {
        $toView = [];
        return View::make('modulos.incidencias.M_TIPOS_VAC.create',$toView);

    }
    public function store(Request $request)
    {
        $CLAVE_TABLA = $request->input('CLAVE_TABLA');

        if(!empty($CLAVE_TABLA)){

            $AN_TAB_VAC = new AN_TAB_VAC();
            $AN_TAB_VAC->CLAVE_TABLA = $request->input('CLAVE_TABLA');
            $AN_TAB_VAC->DESCRIPCION = $request->input('DESCRIPCION');

            DB::transaction(function () use ($AN_TAB_VAC) {

                $AN_TAB_VAC->save();
            });

            return redirect('incidencias/M_TIPOS_VAC/?CLAVE_TABLA='.$CLAVE_TABLA);
        }

    }


    public function nuevo_create(Request $request)
    {

        $AN_TAB_VAC_DET1 = new AN_TAB_VAC_DET1();
        $CLAVE_TABLA = $request->input('CLAVE_TABLA');

        $maxConsec = $AN_TAB_VAC_DET1::where('CLAVE_TABLA',$CLAVE_TABLA)->max("CONSEC");

        $AN_TAB_VAC_DET1->CLAVE_TABLA =$CLAVE_TABLA;
        $AN_TAB_VAC_DET1->CONSEC =($maxConsec + 1);
        $AN_TAB_VAC_DET1->ANOS =$request->input('ANOS');
        $AN_TAB_VAC_DET1->DIAS =$request->input('DIAS');
        $AN_TAB_VAC_DET1->PRIMA =$request->input('PRIMA');
        $AN_TAB_VAC_DET1->DIAS_SOBRE =$request->input('DIAS_SOBRE');

        DB::transaction(function () use($AN_TAB_VAC_DET1)
        {
            $AN_TAB_VAC_DET1->save();
        });

        return redirect('incidencias/M_TIPOS_VAC/?CLAVE_TABLA='.$CLAVE_TABLA)->with('alert-success', trans('La acción se realizó con éxito'));

    }

    public function show($id)
    {
    }
    public function edit($id)
    {
    }
    public function update(Request $request, $id)
    {
    }
    public function destroy($id)
    {
    }

}