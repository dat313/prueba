<?php
/**
 * Created by PhpStorm.
 * User: aicruz
 * Date: 05/09/2017
 * Time: 12:40 PM
 */

namespace App\Http\Controllers\Incidencias;

use App\GmpLib\Helpers;

use App\Models\AN_TAB_VAC;
use App\Models\AN_TAB_VAC_DET1;
use App\Models\AN_TAB_GRAT;
use App\Models\AN_TAB_GRAT_DET1;
use App\Models\OnwLog;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Proengsoft\JsValidation\Facades\JsValidatorFacade;


class M_TIPOS_GRATController extends Controller
{
    public function index(Request $request)
    {
        $CLAVE_TABLA = '';

        if(!empty($request->input('CLAVE_TABLA'))){


            $CLAVE_TABLA = $request->input('CLAVE_TABLA');
        }

        $toView = [
            "CLAVE_TABLA" => $CLAVE_TABLA
        ];

        $AN_TAB_GRAT_DET1 = AN_TAB_GRAT_DET1::where('CLAVE_TABLA',$CLAVE_TABLA)->get();
        $AN_TAB_GRAT =  AN_TAB_GRAT::all();
        $toView['AN_TAB_GRAT'] = $AN_TAB_GRAT;
        $toView['AN_TAB_GRAT_DET1'] = $AN_TAB_GRAT_DET1;

        return View::make('modulos.incidencias.M_TIPOS_GRAT.index',$toView);
    }



    protected function getValidations()
    {
    }
    public function create(Request $request)
    {
        $toView = [];
        return View::make('modulos.incidencias.M_TIPOS_GRAT.create',$toView);

    }
    public function store(Request $request)
    {
        $CLAVE_TABLA = $request->input('CLAVE_TABLA');

        if(!empty($CLAVE_TABLA)){

            $AN_TAB_GRAT = new AN_TAB_GRAT();
            $AN_TAB_GRAT->CLAVE_TABLA = $request->input('CLAVE_TABLA');
            $AN_TAB_GRAT->DESCRIPCION = $request->input('DESCRIPCION');

            DB::transaction(function () use ($AN_TAB_GRAT) {

                $AN_TAB_GRAT->save();
            });

            return redirect('incidencias/M_TIPOS_GRAT/?CLAVE_TABLA='.$CLAVE_TABLA);
        }


    }


    public function nuevo_create(Request $request)
    {
        $AN_TAB_GRAT_DET1 = new AN_TAB_GRAT_DET1();
        $CLAVE_TABLA = $request->input('CLAVE_TABLA');

        $maxConsec = $AN_TAB_GRAT_DET1::where('CLAVE_TABLA',$CLAVE_TABLA)->max("CONSEC");

        $AN_TAB_GRAT_DET1->CLAVE_TABLA =$CLAVE_TABLA;
        $AN_TAB_GRAT_DET1->CONSEC =($maxConsec + 1);
        $AN_TAB_GRAT_DET1->LIM_SUP =$request->input('LIM_SUP');
        $AN_TAB_GRAT_DET1->LIM_INF =$request->input('LIM_INF');
        $AN_TAB_GRAT_DET1->DIAS =$request->input('DIAS');


        DB::transaction(function () use($AN_TAB_GRAT_DET1)
        {
            $AN_TAB_GRAT_DET1->save();
        });

        return redirect('incidencias/M_TIPOS_GRAT/?CLAVE_TABLA='.$CLAVE_TABLA)->with('alert-success', trans('La acción se realizó con éxito'));
    }

    public function show($id)
    {
    }
    public function edit($id)
    {
    }
    public function update(Request $request, $id)
    {
    }
    public function destroy($id)
    {
    }

}