<?php

namespace App\Http\Controllers\Incidencias;

use App\GmpLib\Helpers;
use App\Models\AN_EMP;
use App\Models\AN_EMP_INCI;
use App\Models\AN_FONACOT;
use App\Models\AN_FONACOT_DET1;
use App\Models\AN_TAB_PAR_INFO;
use App\Models\OnwLog;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Proengsoft\JsValidation\Facades\JsValidatorFacade;

class M_FONACOTController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //iniciar variables
        $toView = [
            "AN_EMP" => new AN_EMP()
        ];

        //Si se buscó al empleado
        if ($request->input('NUM_EMP', false)) {

            //sacar el empleado de AN_EMP
            $AN_EMP = AN_EMP::find($request->input('NUM_EMP', false));
            //buscar su número de afiliación
            $AN_FONACOT = AN_FONACOT::findOrNew($AN_EMP->NUM_EMP);
            $toView["AN_EMP"] = $AN_EMP;
            $toView["AN_FONACOT"] = $AN_FONACOT;

        }

        return View::make('modulos.incidencias.M_FONACOT.index', $toView);
    }

    protected function getValidations()
    {
        return [
            "NUM_CREDITO" => "required",
            "NUM_EMP" => "required",
            "IMPORTE" => "required|numeric",
            "RETENCION" => "required|numeric",
            "MESES" => "required|numeric|max:48",
            "F_INICIO" => "required|date_format:d/m/Y|before:" . date("Y-m-d", time() + (3600 * 24))
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $AN_EMP = AN_EMP::findOrFail($request->input('NUM_EMP'));

        $toView = [
            "AN_EMP" => $AN_EMP,
            "validator" => JsValidatorFacade::make($this->getValidations())
        ];
        return View::make('modulos.incidencias.M_FONACOT.create', $toView);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $AN_EMP = AN_EMP::find($request->input('NUM_EMP'));

        $maxConsec = AN_FONACOT_DET1::where("NUM_EMP", $AN_EMP->NUM_EMP)
            ->max("CONSEC");


        $AN_FONACOT_DET1 = new AN_FONACOT_DET1();

        $AN_FONACOT_DET1->NUM_EMP = $AN_EMP->NUM_EMP;
        $AN_FONACOT_DET1->CONSEC = ($maxConsec + 1);
        $AN_FONACOT_DET1->NUM_CREDITO = $request->input('NUM_CREDITO');
        $AN_FONACOT_DET1->IMPORTE = $request->input('IMPORTE');
        $AN_FONACOT_DET1->RETENCION = $request->input('RETENCION');
        $AN_FONACOT_DET1->MESES = $request->input('MESES');
        $AN_FONACOT_DET1->F_INICIO = Helpers::fechaViewToOpen($request->input('F_INICIO'));


        DB::transaction(function () use ($AN_EMP, $AN_FONACOT_DET1) {

            //GUARDAR EL CREDITO
            $AN_FONACOT_DET1->save();

            //CREAR INCIDENCIA
            $obj = AN_EMP_INCI::create([
                "NUM_EMP" => $AN_EMP->NUM_EMP,
                "TIPO_NOM" => Helpers::getAsignacionTIPO_NOM()->TIPO_NOM,
                "CLV_DIP" => AN_TAB_PAR_INFO::where("TIPO_NOM", Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)->first()->CLV_DIP_FONA,
                "PARAM1" => $AN_FONACOT_DET1->RETENCION / $AN_FONACOT_DET1->MESES,
                "PARAM2" => $AN_FONACOT_DET1->IMPORTE,
                "PARAM3_DESDE" => $AN_FONACOT_DET1->F_INICIO,
                "PARAM3_HASTA" => "",
                "REFERENCIA" => $AN_FONACOT_DET1->NUM_CREDITO,
                "CCOSTO" => "",
                "TABULADOR" => "",
            ]);


            //GUARDAR LOG DE INCIDENCIA
            Helpers::bitacora("AN_EMP_INCI",
                ["NUM_EMP" => $AN_EMP->NUM_EMP, "TIPO_NOM" => Helpers::getAsignacionTIPO_NOM()->TIPO_NOM],
                [], $obj->toArray());
            //GUARDAR LOG DE CREDITO
            Helpers::bitacora("AN_FONACOT_DET1",
                ["NUM_EMP" => $AN_EMP->NUM_EMP, "TIPO_NOM" => Helpers::getAsignacionTIPO_NOM()->TIPO_NOM],
                [], $AN_FONACOT_DET1->toArray());
        });
        return redirect('incidencias/M_FONACOT?NUM_EMP=' . $AN_EMP->NUM_EMP)->with('alert-success', trans('La acción se realizó con éxito'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $idPartes = explode("-", $id);

        $NUM_EMP = $idPartes[0];
        $CONSEC = $idPartes[1];

        $toView = [
            "AN_EMP" => AN_EMP::find($NUM_EMP),
            "obj" => AN_FONACOT_DET1::where("NUM_EMP", $NUM_EMP)
                ->where("CONSEC", $CONSEC)->first(),
            "validator" => JsValidatorFacade::make($this->getValidations())
        ];

        return view('modulos.incidencias.M_FONACOT.edit', $toView);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $idPartes = explode("-", $id);

        $NUM_EMP = $idPartes[0];
        $CONSEC = $idPartes[1];
        DB::transaction(function () use ($request, $id) {


            $idPartes = explode("-", $id);

            $NUM_EMP = $idPartes[0];
            $CONSEC = $idPartes[1];


            $AN_FONACOT_DET1_olc = AN_FONACOT_DET1::where("NUM_EMP", $NUM_EMP)
                ->where("CONSEC", $CONSEC)
                ->first();
            AN_FONACOT_DET1::where("NUM_EMP", $NUM_EMP)
                ->where("CONSEC", $CONSEC)
                ->update([
                    "NUM_CREDITO" => $request->input('NUM_CREDITO'),
                    "IMPORTE" => $request->input('IMPORTE'),
                    "RETENCION" => $request->input('RETENCION'),
                    "MESES" => $request->input('MESES'),
                    "F_INICIO" => Helpers::fechaViewToOpen($request->input('F_INICIO')),
                ]);

            $AN_FONACOT_DET1 = AN_FONACOT_DET1::where("NUM_EMP", $NUM_EMP)
                ->where("CONSEC", $CONSEC)
                ->first();


            $DIP = AN_TAB_PAR_INFO::where("TIPO_NOM", Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)->first()->CLV_DIP_FONA;

            $incidenciaOld = AN_EMP_INCI::where("NUM_EMP", $NUM_EMP)
                ->where("TIPO_NOM", Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)
                ->where("CLV_DIP", $DIP)
                ->where("REFERENCIA", $AN_FONACOT_DET1_olc->NUM_CREDITO)
                ->first();
            AN_EMP_INCI::where("NUM_EMP", $NUM_EMP)
                ->where("TIPO_NOM", Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)
                ->where("CLV_DIP", $DIP)
                ->where("REFERENCIA", $AN_FONACOT_DET1_olc->NUM_CREDITO)
                ->update([
                    "CLV_DIP" => $DIP,
                    "PARAM1" => $AN_FONACOT_DET1->RETENCION / $AN_FONACOT_DET1->MESES,
                    "PARAM2" => $AN_FONACOT_DET1->IMPORTE,
                    "PARAM3_DESDE" => $AN_FONACOT_DET1->F_INICIO,
                    "PARAM3_HASTA" => "",
                    "REFERENCIA" => $AN_FONACOT_DET1->NUM_CREDITO
                ]);

            //GUARDAR LOG DE INCIDENCIA
            Helpers::bitacora("AN_EMP_INCI",
                ["NUM_EMP" => $NUM_EMP, "TIPO_NOM" => Helpers::getAsignacionTIPO_NOM()->TIPO_NOM, "CLV_DIP" => $DIP, "REFERENCIA" => $AN_FONACOT_DET1->NUM_CREDITO],
                $incidenciaOld->toArray(), AN_EMP_INCI::where("NUM_EMP", $NUM_EMP)
                    ->where("TIPO_NOM", Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)
                    ->where("CLV_DIP", $DIP)
                    ->where("REFERENCIA", $AN_FONACOT_DET1->NUM_CREDITO)
                    ->first()->toArray());
            //GUARDAR LOG DE CREDITO
            Helpers::bitacora("AN_FONACOT_DET1",
                ["NUM_EMP" => $NUM_EMP, "CONSEC" => $AN_FONACOT_DET1->CONSEC],
                $AN_FONACOT_DET1_olc->toArray(), $AN_FONACOT_DET1->toArray());
        });

        return redirect('incidencias/M_FONACOT?NUM_EMP=' . $NUM_EMP)->with('alert-success', trans('La acción se realizó con éxito'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idPartes = explode("-", $id);

        $NUM_EMP = $idPartes[0];
        $CONSEC = $idPartes[1];

        $AN_FONACOT_DET1 = AN_FONACOT_DET1::where("NUM_EMP", $NUM_EMP)
            ->where("CONSEC", $CONSEC)
            ->first();

        $actuales = AN_FONACOT_DET1::where("NUM_EMP", $NUM_EMP)
            ->get()->toArray();

        $old = $actuales;

        array_forget($actuales, [($CONSEC - 1)]);


        DB::transaction(function () use ($old, $NUM_EMP, $actuales, $AN_FONACOT_DET1) {

            AN_FONACOT_DET1::where("NUM_EMP", $NUM_EMP)
                ->delete();

            $consec = 1;
            foreach ($actuales as $arr) {
                //actualizar consecutivo y sumarle uno después
                $arr["CONSEC"] = $consec++;

                //actualizar con el objeto anterior
                AN_FONACOT_DET1::create($arr);
            }

            $DIP = AN_TAB_PAR_INFO::where("TIPO_NOM", Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)->first()->CLV_DIP_FONA;
            $incidencia = AN_EMP_INCI::where("NUM_EMP", $NUM_EMP)
                ->where("TIPO_NOM", Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)
                ->where("CLV_DIP", $DIP)
                ->where("REFERENCIA", $AN_FONACOT_DET1->NUM_CREDITO)
                ->first();

            $oldInci = $incidencia->toArray();

            AN_EMP_INCI::where("NUM_EMP", $NUM_EMP)
                ->where("TIPO_NOM", Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)
                ->where("CLV_DIP", $DIP)
                ->where("REFERENCIA", $AN_FONACOT_DET1->NUM_CREDITO)
                ->delete();


            //GUARDAR LOG DE INCIDENCIA
            Helpers::bitacora("AN_EMP_INCI",
                ["NUM_EMP" => $NUM_EMP, "TIPO_NOM" => Helpers::getAsignacionTIPO_NOM()->TIPO_NOM, "CLV_DIP" => $DIP, "REFERENCIA" => $AN_FONACOT_DET1->NUM_CREDITO],
                $oldInci, []);
            //GUARDAR LOG DE CREDITO
            Helpers::bitacora("AN_FONACOT_DET1",
                ["NUM_EMP" => $NUM_EMP, "CONSEC" => $AN_FONACOT_DET1->CONSEC],
                $AN_FONACOT_DET1->toArray(), []);

        });

        return redirect()->back()->with('alert-success', trans('La acción se realizó con éxito'));
    }
}
