<?php

namespace App\Http\Controllers\Incidencias;

use App\GmpLib\Helpers;
use App\Http\Controllers\Base\AN_DIPSController;
use App\Http\Controllers\Base\AN_EMP_INCIController;
use App\Http\Controllers\Base\AN_PARAM_INCAController;
use App\Http\Controllers\Base\AN_RECINTOSController;
use App\Http\Controllers\Base\AN_TAB_CCOSTOController;
use App\Http\Controllers\Base\AN_TABULADOR_SALController;
use App\Models\AN_CAL_PERIODOS;
use App\Models\AN_EMP;
use App\Models\AN_EMP_INCI;
use App\Models\OnwLog;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class M_FALT_EMPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $toView = [];
        $AN_RECINTOS_CONTROLLER = new AN_RECINTOSController();
        $AN_PARAM_INCA = new AN_PARAM_INCAController();
        $toView["recintos"] = $AN_RECINTOS_CONTROLLER->index($request);
        $toView["falta_tipos"] = $AN_PARAM_INCA->index($request);

        return View::make('modulos.incidencias.m_falt_emp', $toView);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $NUM_EMP = $request->input("NUM_EMP");
        $incidencias = $request->input("incidencias");

        DB::transaction(function () use ($NUM_EMP, $incidencias) {

            /*AN_EMP_INCI::where("NUM_EMP", $NUM_EMP)
                ->where("TIPO_NOM",
                    Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)->delete();*/

            $data = AN_EMP_INCI::where("NUM_EMP", $NUM_EMP)
                ->where("TIPO_NOM",
                    Helpers::getAsignacionTIPO_NOM()->TIPO_NOM)
                ->get();

            $oldArr = [];
            foreach ($data as $dat) {
                $oldArr[] = $dat->toArray();
                $dat->delete();
            }

            $newArr = [];
            foreach ($incidencias as $arr) {
                $obj = AN_EMP_INCI::create([
                    "NUM_EMP" => $NUM_EMP,
                    "TIPO_NOM" => Helpers::getAsignacionTIPO_NOM()->TIPO_NOM,
                    "CLV_DIP" => Helpers::getArrVal("CLV_DIP",$arr),
                    "PARAM1" => Helpers::getArrVal("PARAM1",$arr,0),
                    "PARAM2" => Helpers::getArrVal("PARAM2",$arr,0),
                    "PARAM3_DESDE" => Helpers::fechaViewToOpen( Helpers::getArrVal("PARAM3_DESDE",$arr)),
                    "PARAM3_HASTA" => Helpers::fechaViewToOpen( Helpers::getArrVal("PARAM3_HASTA",$arr)),
                    "REFERENCIA" => Helpers::getArrVal("REFERENCIA",$arr),
                    "CCOSTO" => Helpers::getArrVal("CCOSTO",$arr),
                    "TABULADOR" => Helpers::getArrVal("TABULADOR",$arr),
                ]);

                $newArr[] = $obj->toArray();
            }
            Helpers::bitacora("AN_EMP_INCI",
                ["NUM_EMP" => $NUM_EMP, "TIPO_NOM" => Helpers::getAsignacionTIPO_NOM()->TIPO_NOM],
                $oldArr, $newArr);
        });
        $AN_EMP_INCI = new AN_EMP_INCIController();
        return $AN_EMP_INCI->index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
