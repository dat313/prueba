<?php

namespace App\Http\Middleware;

use App\Models\F10BaseBitacoras;
use Closure;
use hisorange\BrowserDetect\Facade\Parser as BrowserDetect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

class BitacoraMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(\Illuminate\Http\Request $request, Closure $next)
    {
        //Sacar todos los parámetros del request
        $info = $request->all();
        //Verificar si tiene password
        if (isset($info["password"])) {
            //Sobreescribir el password para no guardarlo como texto plano
            $info["password"] = "***";
        }
//        $obj = F10BaseBitacoras::create([
//            "metodo" => $request->method(),
//            "referer" => Request::server('HTTP_REFERER') ? Request::server('HTTP_REFERER') : "",
//            "ip" => $request->ip(),
//            "request_info" => json_encode($info),
//            "ruta" => $request->path(),
//            "browser_info" => json_encode(BrowserDetect::detect()),
//            //todo fix pantalla
//            "pantalla" => Request::input("X-GMPSIS-PANTALLA") ? Request::input("X-GMPSIS-PANTALLA") : "",
//            "user_id" => Auth::check() ? Auth::user()->id : 0,
//        ]);
//
//        session(["f10_base_bitacora_id" => $obj->id]);

        return $next($request);
    }
}
