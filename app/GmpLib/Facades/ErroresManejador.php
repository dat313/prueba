<?php
/**
 * Created by PhpStorm.
 * User: aicruz
 * Date: 01/09/2015
 * Time: 05:38 PM
 */

namespace App\GmpLib\Facades;

use Illuminate\Support\Facades\Facade;
class ErroresManejador extends Facade
{

    public static function getFacadeAccessor(){

        return 'ErroresManejador';
    }
}