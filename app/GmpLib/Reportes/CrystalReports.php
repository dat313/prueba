<?php

namespace App\GmpLib\Reportes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use App\Soap\Request\GetConversionAmount;
use App\Soap\Response\GetConversionAmountResponse;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class CrystalReports
{
    /**
     * @var SoapWrapper
     */
    protected $soapWrapper;

    protected $pdfData;

    /**
     * SoapController constructor.
     *
     * @param SoapWrapper $soapWrapper
     */
    public function __construct(SoapWrapper $soapWrapper = null)
    {
        if($soapWrapper === null){

            $this->soapWrapper = new SoapWrapper();
        }else{

            $this->soapWrapper = $soapWrapper;
        }
    }

    public function generar($jsonTableRows,$route,$nameTable)
    {
        $Codifica = $this->encriptar($jsonTableRows, '093N10');
        $this->soapWrapper->add('WebServices', function ($service) {
            $service
                ->wsdl(env('CRYSTALREPORTS_WSDL_URL'))
                ->trace(true)
                ->classmap([
                    GetConversionAmount::class,
                    GetConversionAmountResponse::class,
                ]);
        });
        $params = array(
            'parameters' => array(
                'JsonTableRows' => $Codifica,
                'Route' => env('CRYSTALREPORTS_REPOSITORIO_URL').$route,
                'NameTable' => $nameTable
            )
        );

        // Without classmap
        $response = $this->soapWrapper->call('WebServices.CARGA_REPORTE', $params);
        $this->pdfData = $response->CARGA_REPORTEResult;

        return $this->pdfData;
    }

    private function encriptar($texto, $llave){
        $byte = mb_convert_encoding($llave, 'ASCII');
        $desKey = md5(utf8_encode($byte), true);
        $desKey .= substr($desKey,0,8);

        $data = mb_convert_encoding($texto, 'ASCII');

        $blocksize = mcrypt_get_block_size('tripledes', 'ecb');
        $paddingSize = $blocksize - (strlen($data) % $blocksize);
        $data .= str_repeat(chr($paddingSize), $paddingSize);

        $encData = mcrypt_encrypt('tripledes', $desKey, $data, 'ecb');

        return base64_encode($encData);
    }

    public function imprimePdf($nombreArchivo = null){
        $ArrayWebService = $this->pdfData;
        if($ArrayWebService->RETURN_CODE <> 0)
        {
            echo($ArrayWebService->RETURN_DESCRIPTION);
        }
        else
        {
            header("Content-type: application/pdf");
            if($nombreArchivo){
                header('Content-Disposition: attachment; filename='.$nombreArchivo.'.pdf');
            }
            echo $ArrayWebService->RETURN_PDF;
        }


    }
}