<?php
/**
 * Created by PhpStorm.
 * User: aicruz
 * Date: 01/09/2015
 * Time: 05:30 PM
 */

namespace App\GmpLib\Config;


class Database
{
    public static function detectarEntorno()
    {
        return env('DB_CONNECTION', 'sqlsrv') == "oracle" ? "oracle" : "mssql";
    }

    public static function esMSSQL()
    {
        return env('DB_CONNECTION', 'sqlsrv') == "oracle" ? false : true;
    }
}