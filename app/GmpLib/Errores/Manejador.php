<?php
/**
 * Created by PhpStorm.
 * User: aicruz
 * Date: 01/09/2015
 * Time: 05:30 PM
 */

namespace App\GmpLib\Errores;


use App\Models\OnwBaseErrores;
use Illuminate\Support\Facades\Auth;

class Manejador
{
    /**
     * Programa para regresar el mensaje de error
     * @param int $codigo
     * @return array
     */
    public static function reportar($codigo = 1,$datos=[], $e = "")
    {
        //todo reportar a rollbar o loggearlo en algún lado
        $var = OnwBaseErrores::find($codigo);


        if(!$var){
            return [
                "mensaje" => "Ocurrió un error ...",
                "codigo" => 0,
                "exception" =>$e
            ];
        }

        $datos = $var->toArray();

        if (Auth::user()) {
            $lang = Auth::user()->lang;
        } else {
            $lang = "es_MX";
        }

        return [
            "mensaje" => $datos[$lang],
            "codigo" => $codigo
        ];
    }
}