<?php
/**
 * Created by PhpStorm.
 * User: aicruz
 * Date: 01/09/2015
 * Time: 05:42 PM
 */

namespace App\GmpLib\ServiceProviders;

use Illuminate\Support\ServiceProvider;

class ErroresManejadorServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind('ErroresManejador', 'App\GmpLib\Errores\Manejador');
    }
}