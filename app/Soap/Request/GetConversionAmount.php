<?php

namespace App\Soap\Request;

class GetConversionAmount
{
  /**
   * @var string
   */
  protected $JsonTableRows;

  /**
   * @var string
   */
  protected $Route;

  /**
   * @var string
   */
  protected $NameTable;

   /**
   * GetConversionAmount constructor.
   *
   * @param string $JsonTableRows
   * @param string $Route
   * @param string $NameTable
   */
  public function __construct($JsonTableRows, $Route, $NameTable)
  {
    $this->JsonTableRows = $JsonTableRows;
    $this->Route         = $Route;
    $this->NameTable     = $NameTable;
  }

  /**
   * @return string
   */
  public function getJsonTableRows()
  {
    return $this->JsonTableRows;
  }

  /**
   * @return string
   */
  public function getRoute()
  {
    return $this->Route;
  }

  /**
   * @return string
   */
  public function getNameTable()
  {
    return $this->NameTable;
  }

}