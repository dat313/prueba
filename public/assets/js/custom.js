/**
 * Created by aicruz on 22/02/2016.
 */
var app = angular.module('app',
    [
        'ngStorage',
        'oitozero.ngSweetAlert',
        'angular.chosen',
        'angular-loading-bar'
    ], function ($interpolateProvider) {
    });
/**
 * Campo NUM_EMP con autocomplete
 *
 * Uso: poner la clase .NUM_EMP en un input[type=text]
 * Y agregar como custom param  AN_EMP_FIELD="#AN_EMP"
 * El parámetro el el selector del campo que tendrá asociado el campo que se utilizará
 */
$(".NUM_EMP").each(function (index, value) {
    $(value).autocomplete({
        serviceUrl: '/base/AN_EMP',
        paramName: 'q',
        dataType: 'json',
        transformResult: function (response) {
            return {
                suggestions: $.map(response, function (dataItem) {
                    return {
                        data: dataItem.NUM_EMP,
                        value: dataItem.NUM_EMP + " / " + dataItem.NOMP_EMP + " " + dataItem.AP_PATER + " " + dataItem.AP_MATER
                    };
                })
            };
        },
        onSelect: function (suggestion) {
            $($(value).attr("AN_EMP_FIELD")).val(suggestion.data);
            $(value).val(suggestion.value);
            angular.element($($(value).attr("AN_EMP_FIELD"))).triggerHandler('change');
            angular.element($(value)).triggerHandler('change');
        }
    });
});
$(".date").mask("99/99/9999", {placeholder: "DD/MM/AAAA"});
$(".rfc").mask("aaaa-999999-***", {placeholder: "XAXX-010101-000"});
$(".imss").mask('99-99-99-9999-9', {placeholder: "00-00-00-0000-0"});
$(".curp").mask('aaaa999999aaaaaa**', {placeholder: ""});
$(".folio_inca").mask('aa******', {placeholder: ""});