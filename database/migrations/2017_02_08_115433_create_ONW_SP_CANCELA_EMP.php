<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateONWSPCANCELAEMP extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (\App\GmpLib\Config\Database::esMSSQL()) {
            \Illuminate\Support\Facades\DB::statement(file_get_contents(database_path('store_procedures/ONW_SP_CANCELA_EMP.sql')));
        } else {
            //todo implementar oracle
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
