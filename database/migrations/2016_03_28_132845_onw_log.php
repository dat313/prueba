<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OnwLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('onw_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tabla');
            $table->string('llave');
            $table->text('llaves_det');
            $table->string('usuario');
            $table->longText('datos_old');
            $table->longText('datos_new');
            $table->unsignedInteger('f10_base_bitacora_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('onw_logs');
    }
}
