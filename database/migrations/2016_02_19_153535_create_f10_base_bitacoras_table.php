<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateF10BaseBitacorasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('f10_base_bitacoras', function (Blueprint $table) {
            $table->increments('id');
            $table->string("metodo",10);
            $table->string("referer");
            $table->string("ip",20);
            $table->text("request_info");
            $table->string("ruta");
            $table->text("browser_info");
            $table->string("pantalla");
            $table->unsignedInteger("user_id");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('f10_base_bitacoras');
    }
}
