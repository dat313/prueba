CREATE PROCEDURE [ONW_SP_CANCELA_EMP] (@psNumEmp VARCHAR(06), @ps_result INT OUTPUT,  @ps_msg VARCHAR(300) OUTPUT)  AS

BEGIN

/*
______________________________________________________________________________________

Nombre de Objeto:   ONW_SP_CANCELA_EMP
Tipo de Objeto:            StoreProcedure
Autor:                     JBM
Objetivo:                  Cancelacion de Alta de empleados, siempre y cuando  aún no se ha enviado movimiento al IMSS
Fecha de Creación:  20170207
Modificaciones:
Fecha               Desarrollador   Notas del cambio
===========     =============  =================================
YYYYMMDD            JBM
_______________________________________________________________________________________
*/
       BEGIN  TRY
             BEGIN TRAN
                    -- Declaración de variables

                    DECLARE @lsNumEmp VARCHAR(06), @lsStatus VARCHAR(10), @liStatus INT, @lsMsg1 VARCHAR(100),@lsMsg2 VARCHAR(100),@lsMsg3 VARCHAR(100)


                    -- Obtener valores de parametros

                    SET @lsNumEmp = @psNumEmp
                    SET @liStatus = 0
                    SET @lsMsg1 = ''
                    SET @lsMsg2 = ''
                    SET @lsMsg3 = ''

                    -- Obtener de an_emp_captura la información a cargar a tablas de OpenHR

                    -- Valida movimientos en AN_HIS_IMSS

                    SELECT TOP 1 @lsStatus =status_imss
                    FROM an_his_imss
                    WHERE num_emp = @psNumEmp

                    SET @lsStatus = '1' --eliminar del SP

                    IF @lsStatus = '0' OR @lsStatus = ''
                           SET @liStatus = 0
                    ELSE
                           BEGIN
                                  SET @liStatus = 1
                                  SET @lsMsg1 = 'No procede la Cancelación de Alta, el empleado tiene Alta en el IMSS.'
                                  SET @lsStatus = '0'
                           END

                    -- Valida movimientos en AN_ACUM
                    SELECT @lsStatus = CONVERT(VARCHAR(10),COUNT(*))
                    FROM an_acum
                    WHERE num_emp = @psNumEmp

                    SET @lsStatus = '1' -- Eliminar del SP

                    IF @lsStatus <> '0'
                           BEGIN
                                  SET @liStatus = 1
                                  SET @lsMsg2 = 'No procede la Cancelación de Alta, el empleado tiene Acumulados.'
                           END


                    IF @liStatus = 0
                                  BEGIN
                                        DELETE FROM an_emp_habilidades WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_emp_habilidades_det1 WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_emp_familiares WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_emp_familiares_det1 WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_emp_familiares_det2 WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_emp_aca_det1 WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_emp_aca WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_emp_idiomas_det1 WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_emp_idiomas WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_emp_capacitacion_det1 WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_emp_capacitacion WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_emp_grales WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_his_imss WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_historico WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_historico_det1 WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_historico_det2 WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_his_puestos WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_his_puestos_det1 WHERE num_emp = @lsNumEmp
                                        DELETE FROM an_emp WHERE num_emp = @lsNumEmp
                                  END


                    SET @ps_result = CONVERT(VARCHAR(10), @lsStatus)
                    SET @ps_msg = @lsMsg1 + ' || ' + @lsMsg2 + ' || ' + @lsMsg3

             COMMIT TRAN
       END TRY

       BEGIN CATCH

             ROLLBACK TRANSACTION
             DECLARE @ls_error_message NVARCHAR(4000);
             DECLARE @li_error_severity INT;
             DECLARE @li_error_state INT;

             SELECT @ls_error_message = ERROR_MESSAGE(), @li_error_severity = ERROR_SEVERITY(), @li_error_state = ERROR_STATE();

             RAISERROR (@ls_error_message, @li_error_severity, @li_error_state );
             PRINT 'Ocurrio un error en el proceso [ALTA_EMP]'

             RETURN @li_error_severity
       END CATCH

       RETURN 0


/*
Zona de consultas adoc al SP

EXEC CANCELA_EMP '9704'


*/


END